docker build \
    --rm \
    --tag mpiaria/nginx:stable \
    .

docker run \
    --detach \
    --name nginx \
    --publish 0.0.0.0:80:80/tcp \
    --publish 0.0.0.0:443:443/tcp \
    --restart=always \
    --volume /data/nginx/usr/share/nginx/html:/usr/share/nginx/html \
    --volume /data/nginx/var/log/nginx:/var/log/nginx \
    mpiaria/nginx:stable

