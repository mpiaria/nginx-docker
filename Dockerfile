FROM nginx:stable
RUN mkdir /etc/nginx/ssl && rm /etc/nginx/conf.d/*
COPY etc/nginx/conf.d/* /etc/nginx/conf.d/
COPY ssl/www.michaeliaria.com/www.michaeliaria.com.chained.crt /etc/nginx/ssl/
COPY ssl/www.michaeliaria.com/www.michaeliaria.com.key /etc/nginx/ssl/
RUN chown root:root /etc/nginx/ssl/www.michaeliaria.com.key \
    && chmod 600 /etc/nginx/ssl/www.michaeliaria.com.key
